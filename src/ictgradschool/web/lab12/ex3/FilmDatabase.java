package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.ex1.Keyboard;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;

/**
 * Created by mmiz318 on 4/01/2018.
 */
public class FilmDatabase {
    public void start() throws IOException, SQLException {
        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get all info about all lecturers
            System.out.println("Welcome to the Film database!");

            int choice = menu();



            if (choice == 1) {
                System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
                String userInput = Keyboard.readInput();

                Result<Record> filmData = create.select().from(PFILMS_ACTOR).where(PFILMS_ACTOR.ACTOR_FNAME.eq(userInput).or(PFILMS_ACTOR.ACTOR_LNAME.eq(userInput))).fetch();

                for(Record record: filmData) {
                    String firstName = record.getValue(PFILMS_ACTOR.ACTOR_FNAME);
                    String lastName = record.getValue(PFILMS_ACTOR.ACTOR_LNAME);
                    System.out.println(firstName + " " + lastName + " is listed as being involved in the following films:");

                    int actorId = record.getValue(PFILMS_ACTOR.ACTOR_ID);

                    Result<Record> films = create.select().from(PFILMS_PARTICIPATES_IN).join(PFILMS_ACTOR).onKey().join(PFILMS_FILM).onKey().join(PFILMS_ROLE).onKey().where(PFILMS_ACTOR.ACTOR_ID.eq(actorId)).fetch();
                    for (Record hi : films) {

                        System.out.println(hi.getValue(PFILMS_FILM.FILM_TITLE) + " (" + hi.getValue(PFILMS_ROLE.ROLE_NAME) + ")");
                    }

                }

            } else if (choice == 2) {
                System.out.println("Please enter the name of the film you wish to get information about, or press enter to return to the previous menu");
                String userInput = Keyboard.readInput();

                Result<Record> filmData = create.select().from(PFILMS_FILM).where(PFILMS_FILM.FILM_TITLE.eq(userInput)).fetch();

                for(Record record: filmData) {
                    String filmName = record.getValue(PFILMS_FILM.FILM_TITLE);
                    String genre = record.getValue(PFILMS_FILM.GENRE_NAME);
                    System.out.println("The film " + filmName + " is a " + genre + " movie that features the following people:");

                    int filmId = record.getValue(PFILMS_FILM.FILM_ID);

                    Result<Record> films = create.select().from(PFILMS_PARTICIPATES_IN).join(PFILMS_ACTOR).onKey().join(PFILMS_FILM).onKey().join(PFILMS_ROLE).onKey().where(PFILMS_FILM.FILM_ID.eq(filmId)).fetch();
                    for (Record hi : films) {
                        System.out.println(hi);
                        System.out.println(hi.getValue(PFILMS_ACTOR.ACTOR_FNAME) + " " + hi.getValue(PFILMS_ACTOR.ACTOR_LNAME) + " (" + hi.getValue(PFILMS_ROLE.ROLE_NAME) + ")");
                    }

                }
            } else if (choice == 3) {
                System.out.println("Please enter the name of the genre you wish to get information about, or press enter to return to the previous menu");
                String userInput = Keyboard.readInput();

                System.out.println("The " +  userInput + " genre includes the following films");
                Result<Record> filmData = create.select().from(PFILMS_FILM).where(PFILMS_FILM.GENRE_NAME.eq(userInput)).fetch();
                for(Record record: filmData) {
                    String genre = record.getValue(PFILMS_FILM.GENRE_NAME);
                    System.out.println(record.getValue(PFILMS_FILM.FILM_TITLE));
//                    Result<Record> films = create.select().from(PFILMS_FILM).where(PFILMS_FILM.FILM_ID.eq(filmId)).fetch();
//                    for (Record hi : films) {
//                        System.out.println(hi);
//                        System.out.println(hi.getValue(PFILMS_ACTOR.ACTOR_FNAME) + " " + hi.getValue(PFILMS_ACTOR.ACTOR_LNAME) + " (" + hi.getValue(PFILMS_ROLE.ROLE_NAME) + ")");
//                    }

                }

            }
        }
    }
    public int menu(){
        System.out.println("Please select an option from the following: ");
        System.out.println("1. Information by Actor");
        System.out.println("2. Information by Movie");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit");

        int userInput = Integer.parseInt(Keyboard.readInput());
        return userInput;
    }

    public static void main(String[] args) throws IOException, SQLException {
        new FilmDatabase().start();
    }
}
