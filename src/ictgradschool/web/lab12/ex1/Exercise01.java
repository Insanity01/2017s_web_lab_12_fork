package ictgradschool.web.lab12.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            new Exercise01().printArticle(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void printArticle(Connection conn) {
        for (int i = 0; i < 5; i++) {

            System.out.println("Please type a title: ");
            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT * FROM simpledao_articles WHERE title LIKE ?;"
            )) {
                String userInput = Keyboard.readInput() + "%";
                stmt.setString(1, userInput);

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        System.out.println(r.getString(r.findColumn("body")));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

